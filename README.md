# Software Studio 2020 Spring Midterm Project


## Topic
* Project Name : Tanuking
* Key functions (add/delete)
    1. chat room
    
* Other functions (add/delete)
    1. public room

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：https://midterm-project-107000109.web.app/

# Components Description : 
1. 主畫面: 
由一個NAVBAR跟底下的幻燈秀組成。幻燈秀是GIF圖片(三張一樣的圖片，字不一樣)
點擊NAVBAR的按鈕，選擇"login"選項，即可跳轉進入login畫面。

![](https://i.imgur.com/KGwStaU.jpg)

![](https://i.imgur.com/CWQxtjP.jpg)


2. Login 畫面: google and facebook及創建新帳號並登入
總共提供三種登入方式，包括facebook，google及可在本站用信箱創建新帳號。
只要輸入欲使用的帳號密碼並按下register即可創建新帳號。

![](https://i.imgur.com/nU1ZyTU.jpg)

3. 登入之後

![](https://i.imgur.com/tBQ4U1a.jpg)

![](https://i.imgur.com/1a4loo1.jpg)

用戶帳號會被顯示在上面，可以選擇要進入publicroom或是private room。

4. private room

想要新增新的聊天室，輸入聊天室名稱後按newroom即可新增。

![](https://i.imgur.com/opr3Xbu.jpg)

點enter可以進入聊天室，聊天室是用collapse功能做的。

![](https://i.imgur.com/DFKPPQ4.jpg)

聊天室的上方可以加入一位好友進來聊天。(這是雙人聊天室)
加入的方法同新增聊天室的方法。

加入後，即可聊天!

![](https://i.imgur.com/4xkXfsb.jpg)


5. css animation : 
   淡入淡出動畫(載入網頁時的特效)
   新增成員及房間按鈕動畫(hover，移動滑鼠到按鈕上，會有浮動效果)。

![](https://i.imgur.com/fFvSh2A.jpg)

6. chrome notification :

只要有人傳遞新訊息，就會出現!

![](https://i.imgur.com/F0QXvnv.jpg)


# Other Functions Description : 

Public room 功能(公共聊天區) : 只要進入公共聊天區的小夥伴都可以看到上面的訊息，也可以發表訊息，所有使用者都看得到您在上面發布的訊息。

## Security Report (Optional)
?