function init() 
{
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) 
    {
        var User = document.getElementById('user');
        // Check user login
        if (user) 
        {
            user_email = user.email;
            User.innerHTML = "<span class='nav-link'>" + user.email;
            var logout = document.getElementById('logout');
            logout.addEventListener('click',function()
            {
                firebase.auth().signOut().then(function() 
                {
                    // Sign-out successful.
                    alert('logout successfully');
                    window.location.assign("./index.html");
                    
                                        
                }).catch(function(error) 
                {
                    // An error happened.
                    alert("logout error");
                });               
            }
            )         
        } 
    });
}

window.onload = function() 
{
    init();
};