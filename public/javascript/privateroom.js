function init() 
{
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) 
    {
        var User = document.getElementById('user');
        // Check user login
        if (user) 
        {
            user_email = user.email;
            User.innerHTML = "<span class='nav-link'>" + user.email;
            var logout = document.getElementById('logout');
            logout.addEventListener('click',function()
            {
                firebase.auth().signOut().then(function() 
                {
                    // Sign-out successful.
                    alert('logout successfully');
                    window.location.assign("./index.html");
                    
                                        
                }).catch(function(error) 
                {
                    // An error happened.
                    alert("logout error");
                });               
            }
            )         
        } 
    });
    // To create new room
    var newroom_btn = document.getElementById('newroom_btn');
    var roomname = document.getElementById('roomname');
    
    var notifyConfig = 
    {
        body: 'you have a new message', // 設定內容
        icon: 'css/photo_src/tanuki.png', // 設定 icon
    };

    newroom_btn.addEventListener('click', function() 
    {
        if (roomname.value != "") 
        {
            firebase.database().ref("room_list").push({
                host: user_email,
                data : roomname.value,
            });
            roomname.value = "";
        }
    });
    
    var roomsRef = firebase.database().ref("room_list");
    // user in the newroom

    ///////////////////////////////////////////////1//////////////////////////////////////////
    var user1_email;
    var usersRef1 = firebase.database().ref("indiv_com_list1_users");
    usersRef1.on('child_added',function(data) 
    {
        var user1 = data.val();
        user1_email = user1.email;
    });

    ///////////////////////////////////////////////2//////////////////////////////////////////

    var user2_email;
    var usersRef2 = firebase.database().ref("indiv_com_list2_users");
    usersRef2.on('child_added',function(data) 
    {
        var user2 = data.val();
        user2_email = user2.email;
    });

    ///////////////////////////////////////////////3//////////////////////////////////////////

    var user3_email;
    var usersRef3 = firebase.database().ref("indiv_com_list3_users");
    usersRef3.on('child_added',function(data) 
    {
        var user3 = data.val();
        user3_email = user3.email;
    });

    ///////////////////////////////////////////////4//////////////////////////////////////////

    var user4_email;
    var usersRef4 = firebase.database().ref("indiv_com_list4_users");
    usersRef4.on('child_added',function(data) 
    {
        var user4 = data.val();
        user4_email = user4.email;
    });

    ///////////////////////////////////////////////5//////////////////////////////////////////

    var user5_email;
    var usersRef5 = firebase.database().ref("indiv_com_list5_users");
    usersRef5.on('child_added',function(data) 
    {
        var user5 = data.val();
        user5_email = user5.email;
    });

    ///////////////////////////////////////////////6//////////////////////////////////////////

    var user6_email;
    var usersRef6 = firebase.database().ref("indiv_com_list6_users");
    usersRef6.on('child_added',function(data) 
    {
        var user6 = data.val();
        user6_email = user6.email;
    });

    ///////////////////////////////////////////////7//////////////////////////////////////////

    var user7_email;
    var usersRef7 = firebase.database().ref("indiv_com_list7_users");
    usersRef7.on('child_added',function(data) 
    {
        var user7 = data.val();
        user7_email = user7.email;
    });

    //////////////////////////////////////////////////////////////////////////////////////////

    var str_after_room = "</p></div></div>\n";
    // List for store posts html
    var total_room = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    
    roomsRef.once('value').then(function(snapshot) 
    {
            total_room.value = snapshot.val();
            //console.log(total_room.value);
            document.getElementById('newroom').innerHTML = total_room.join('');
            /// Add listener to update new post
            roomsRef.on('child_added', function(data) 
            {
                second_count += 1;
                if (second_count > first_count) 
                {
                    var childData = data.val();
                    ///////////////////////////////////////1////////////////////////////////////////
                    if((childData.host == user_email || user1_email == user_email) && second_count == 1)
                    {

                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom1' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');

                    }
                    ///////////////////////////////////////2////////////////////////////////////////

                    else if((childData.host == user_email || user2_email == user_email) && second_count == 2)
                    {

                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom2' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');
                     
                    }

                    ///////////////////////////////////////3////////////////////////////////////////

                    else if((childData.host == user_email || user3_email == user_email) && second_count == 3)
                    {
                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom3' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');
                    }

                    ///////////////////////////////////////4////////////////////////////////////////

                    else if((childData.host == user_email || user4_email == user_email) && second_count == 4)
                    {

                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom4' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');

                    }

                    ///////////////////////////////////////5////////////////////////////////////////

                    else if((childData.host == user_email || user5_email == user_email) && second_count == 5)
                    {
                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom5' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');

                    }

                    ///////////////////////////////////////6////////////////////////////////////////

                    else if((childData.host == user_email || user6_email == user_email) && second_count == 6)
                    {
                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom6' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');
                    }

                    ///////////////////////////////////////7////////////////////////////////////////

                    else if((childData.host == user_email || user7_email == user_email) && second_count == 7)
                    {
                            var childData = data.val();
                            var str_before_roomname = "<div class='my-3 p-3 bg-white rounded box-shadow w-100 float-center'><div class='media text-muted pt-3'><p><button class='btn btn-outline-info btn btn-info' type='button' data-toggle='collapse' data-target='#individualroom7' aria-expanded='false' aria-controls='collapseExample'>enter</button></p><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>"; 
                            total_room[total_room.length] = str_before_roomname  + childData.host + "</strong>" + childData.data + str_after_room
                            document.getElementById('newroom').innerHTML = total_room.join('');
                    }
                
                    
                    ///////////////////////////////////////////////1//////////////////////////////////////////

                    var addfriend_btn1 = document.getElementById('addfriend_btn1');
                    var friend1 = document.getElementById('addfriend1');
                    addfriend_btn1.addEventListener('click', function() 
                    {
                        if (friend1.value != "") 
                        {
                            //console.log("1");
                            firebase.database().ref("indiv_com_list1_users").push({
                                email: friend1.value,
                            }); 
                            friend1.value = "";
                        }
                    });     
                    send_btn1 = document.getElementById('send1');
                    text1 = document.getElementById('textbox1');
                    send_btn1.addEventListener('click', function() 
                    {
                        if (text1.value != "") 
                        {
                            var txt = text1.value;
                            firebase.database().ref("indiv_com_list1").push({
                                email: user_email,
                                data : txt,
                            }); 
                            text1.value = "";
                        }
                    });

                    ///////////////////////////////////////////////2//////////////////////////////////////////

                    var addfriend_btn2 = document.getElementById('addfriend_btn2');
                    var friend2 = document.getElementById('addfriend2')
                    addfriend_btn2.addEventListener('click', function() 
                    {
                        if (friend2.value != "") 
                        {
                            //console.log("2");
                            firebase.database().ref("indiv_com_list2_users").push({
                                email: friend2.value,
                            }); 
                            friend2.value = "";
                        }
                    });

                    send_btn2 = document.getElementById('send2');
                    text2 = document.getElementById('textbox2');
                    send_btn2.addEventListener('click', function() 
                    {
                        if (text2.value != "") 
                        {
                            var txt = text2.value;
                            firebase.database().ref("indiv_com_list2").push({
                                email: user_email,
                                data : txt,
                            }); 
                            text2.value = "";
                        }
                    });

                    ///////////////////////////////////////////////3//////////////////////////////////////////
                    
                    var addfriend_btn3 = document.getElementById('addfriend_btn3');
                    var friend3 = document.getElementById('addfriend3')
                    addfriend_btn3.addEventListener('click', function() 
                    {
                        if (friend3.value != "") 
                        {
                            //console.log("2");
                            firebase.database().ref("indiv_com_list3_users").push({
                                email: friend3.value,
                            }); 
                            friend3.value = "";
                        }
                    });
                    
                    send_btn3 = document.getElementById('send3');
                    text3 = document.getElementById('textbox3');
                    send_btn3.addEventListener('click', function() 
                    {
                        if (text3.value != "") 
                        {
                            var txt = text3.value;
                            firebase.database().ref("indiv_com_list3").push({
                                email: user_email,
                                data : txt
                            }); 
                            text3.value = "";
                        }
                    });

                    ///////////////////////////////////////////////4//////////////////////////////////////////

                    var addfriend_btn4 = document.getElementById('addfriend_btn4');
                    var friend4 = document.getElementById('addfriend4')
                    addfriend_btn4.addEventListener('click', function() 
                    {
                        if (friend4.value != "") 
                        {
                            //console.log("2");
                            firebase.database().ref("indiv_com_list4_users").push({
                                email: friend4.value,
                            }); 
                            friend4.value = "";
                        }
                    });
                    
                    send_btn4 = document.getElementById('send4');
                    text4 = document.getElementById('textbox4');
                    send_btn4.addEventListener('click', function() 
                    {
                        if (text4.value != "") 
                        {
                            var txt = text4.value;
                            firebase.database().ref("indiv_com_list4").push({
                                email: user_email,
                                data : txt
                            }); 
                            text3.value = "";
                        }
                    });

                    ///////////////////////////////////////////////5//////////////////////////////////////////

                    var addfriend_btn5 = document.getElementById('addfriend_btn5');
                    var friend5 = document.getElementById('addfriend5')
                    addfriend_btn5.addEventListener('click', function() 
                    {
                        if (friend5.value != "") 
                        {
                            //console.log("2");
                            firebase.database().ref("indiv_com_list5_users").push({
                                email: friend5.value,
                            }); 
                            friend5.value = "";
                        }
                    });
                    
                    send_btn5 = document.getElementById('send5');
                    text5 = document.getElementById('textbox5');
                    send_btn5.addEventListener('click', function() 
                    {
                        if (text5.value != "") 
                        {
                            var txt = text5.value;
                            firebase.database().ref("indiv_com_list5").push({
                                email: user_email,
                                data : txt
                            }); 
                            text5.value = "";
                        }
                    });

                    ///////////////////////////////////////////////6//////////////////////////////////////////

                    var addfriend_btn6 = document.getElementById('addfriend_btn6');
                    var friend6 = document.getElementById('addfriend6')
                    addfriend_btn6.addEventListener('click', function() 
                    {
                        if (friend6.value != "") 
                        {
                            //console.log("2");
                            firebase.database().ref("indiv_com_list6_users").push({
                                email: friend6.value,
                            }); 
                            friend6.value = "";
                        }
                    });
                    
                    send_btn6 = document.getElementById('send6');
                    text6 = document.getElementById('textbox6');
                    send_btn6.addEventListener('click', function() 
                    {
                        if (text6.value != "") 
                        {
                            var txt = text6.value;
                            firebase.database().ref("indiv_com_list6").push({
                                email: user_email,
                                data : txt
                            }); 
                            text6.value = "";
                        }
                    });

                    ///////////////////////////////////////////////7//////////////////////////////////////////

                    var addfriend_btn7 = document.getElementById('addfriend_btn7');
                    var friend7 = document.getElementById('addfriend7')
                    addfriend_btn7.addEventListener('click', function() 
                    {
                        if (friend7.value != "") 
                        {
                            //console.log("2");
                            firebase.database().ref("indiv_com_list7_users").push({
                                email: friend7.value,
                            }); 
                            friend7.value = "";
                        }
                    });
                    
                    send_btn7 = document.getElementById('send7');
                    text7 = document.getElementById('textbox7');
                    send_btn7.addEventListener('click', function() 
                    {
                        if (text7.value != "") 
                        {
                            var txt = text7.value;
                            firebase.database().ref("indiv_com_list7").push({
                                email: user_email,
                                data : txt
                            }); 
                            text7.value = "";
                        }
                    });

                    ///////////////////////////////////// The html code for post//////////////////////////////
                    
                    ///////////////////////////////////////////////1//////////////////////////////////////////
                    var postsRef1 = firebase.database().ref("indiv_com_list1");   
                    var str_before_username_3 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_4 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post1 = [];
                    // Counter for checking history post update complete
                    var first_count_1 = 0;
                    // Counter for checking when to update new post
                    var second_count_1 = 0;
                
                    postsRef1.once('value').then(function(snapshot) 
                    {
                        total_post1.value = snapshot.val();
                        document.getElementById('individual1').innerHTML = total_post1.join('');
                        
                            /// Add listener to update new post
                        postsRef1.on('child_added', function(data) 
                        {
                            second_count_1 += 1;
                            if (second_count_1 > first_count_1) 
                            {
                                var childData = data.val();
                                    //console.log(user_email + "1");
                                if(childData.email == user_email)
                                {
                                    total_post1[total_post1.length] = str_before_username_3 + childData.email + "</strong>" + childData.data + str_after_content
                                }
                                else
                                {
                                    //console.log("2");
                                    total_post1[total_post1.length] = str_before_username_4 + childData.email + "</strong>" + childData.data + str_after_content
                                    if(user_email == user1_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                                }           
                                document.getElementById('individual1').innerHTML = total_post1.join('');     
                            }
                        });
                    })

                    ////////////////////////////////////////2////////////////////////////////////

                    var postsRef2 = firebase.database().ref("indiv_com_list2");
                    var str_before_username_5 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_6 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post2 = [];
                    // Counter for checking history post update complete
                    var first_count_2 = 0;
                    // Counter for checking when to update new post
                    var second_count_2 = 0;
                
                    postsRef2.once('value').then(function(snapshot) 
                    {
                        total_post2.value = snapshot.val();
                        document.getElementById('individual2').innerHTML = total_post2.join('');
                        
                        /// Add listener to update new post
                        postsRef2.on('child_added', function(data) 
                        {
                            second_count_2 += 1;
                            if (second_count_2 > first_count_2) 
                            {
                                var childData = data.val();
                                if(childData.email == user_email)
                                    total_post2[total_post2.length] = str_before_username_5 + childData.email + "</strong>" + childData.data + str_after_content
                                else
                                {
                                    total_post2[total_post2.length] = str_before_username_6 + childData.email + "</strong>" + childData.data + str_after_content
                                    if(user_email == user2_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                               }           
                                document.getElementById('individual2').innerHTML = total_post2.join('');     
                            }
                        });
                    })

                    ////////////////////////////////////////3////////////////////////////////////

                    var postsRef3 = firebase.database().ref("indiv_com_list3");
                    var str_before_username_7 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_8 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post3 = [];
                    // Counter for checking history post update complete
                    var first_count_3 = 0;
                    // Counter for checking when to update new post
                    var second_count_3 = 0;
                
                    postsRef3.once('value').then(function(snapshot) 
                    {
                        total_post3.value = snapshot.val();
                        document.getElementById('individual3').innerHTML = total_post3.join('');

                        /// Add listener to update new post
                        postsRef3.on('child_added', function(data) 
                        {
                            second_count_3 += 1;
                            if (second_count_3 > first_count_3) 
                            {
                                var childData = data.val();
                                if(childData.email == user_email)
                                    total_post3[total_post3.length] = str_before_username_7 + childData.email + "</strong>" + childData.data + str_after_content
                                else
                                {
                                    total_post3[total_post3.length] = str_before_username_8 + childData.email + "</strong>" + childData.data + str_after_content
                                                            
                                    if(user_email == user3_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                                }           
                                document.getElementById('individual3').innerHTML = total_post3.join('');     
                            }
                        });
                    })
                    ////////////////////////////////////////4////////////////////////////////////

                    var postsRef4 = firebase.database().ref("indiv_com_list4");
                    var str_before_username_9 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_10 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post4 = [];
                    // Counter for checking history post update complete
                    var first_count_4 = 0;
                    // Counter for checking when to update new post
                    var second_count_4 = 0;
                
                    postsRef4.once('value').then(function(snapshot) 
                    {
                        total_post4.value = snapshot.val();
                        document.getElementById('individual4').innerHTML = total_post4.join('');

                        /// Add listener to update new post
                        postsRef4.on('child_added', function(data) 
                        {
                            second_count_4 += 1;
                            if (second_count_4 > first_count_4) 
                            {
                                var childData = data.val();
                                if(childData.email == user_email)
                                    total_post4[total_post4.length] = str_before_username_9 + childData.email + "</strong>" + childData.data + str_after_content
                                else
                                {
                                    total_post4[total_post4.length] = str_before_username_10 + childData.email + "</strong>" + childData.data + str_after_content
                                                            
                                    if(user_email == user4_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                                }           
                                document.getElementById('individual4').innerHTML = total_post4.join('');     
                            }
                        });
                    })
                    ////////////////////////////////////////5////////////////////////////////////

                    var postsRef5 = firebase.database().ref("indiv_com_list5");   
                    var str_before_username_11 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_12 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post5 = [];
                    // Counter for checking history post update complete
                    var first_count_5 = 0;
                    // Counter for checking when to update new post
                    var second_count_5 = 0;
                
                    postsRef5.once('value').then(function(snapshot) 
                    {
                        total_post5.value = snapshot.val();
                        document.getElementById('individual5').innerHTML = total_post5.join('');
                        
                            /// Add listener to update new post
                        postsRef5.on('child_added', function(data) 
                        {
                            second_count_5 += 1;
                            if (second_count_5 > first_count_5) 
                            {
                                var childData = data.val();
                                    //console.log(user_email + "1");
                                if(childData.email == user_email)
                                {
                                    total_post5[total_post5.length] = str_before_username_11 + childData.email + "</strong>" + childData.data + str_after_content
                                }
                                else
                                {
                                    //console.log("2");
                                    total_post5[total_post5.length] = str_before_username_12 + childData.email + "</strong>" + childData.data + str_after_content
                                    if(user_email == user5_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                                }           
                                document.getElementById('individual5').innerHTML = total_post5.join('');     
                            }
                        });
                    })

                    ////////////////////////////////////////6////////////////////////////////////

                    var postsRef6 = firebase.database().ref("indiv_com_list6");   
                    var str_before_username_13 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_14 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post6 = [];
                    // Counter for checking history post update complete
                    var first_count_6 = 0;
                    // Counter for checking when to update new post
                    var second_count_6 = 0;
                
                    postsRef6.once('value').then(function(snapshot) 
                    {
                        total_post6.value = snapshot.val();
                        document.getElementById('individual6').innerHTML = total_post6.join('');
                        
                       
                            /// Add listener to update new post
                        postsRef6.on('child_added', function(data) 
                        {
                            second_count_6 += 1;
                            if (second_count_6 > first_count_6) 
                            {
                                var childData = data.val();
                                    //console.log(user_email + "1");
                                if(childData.email == user_email)
                                {
                                    total_post6[total_post6.length] = str_before_username_13 + childData.email + "</strong>" + childData.data + str_after_content
                                }
                                else
                                {
                                    //console.log("2");
                                    total_post6[total_post6.length] = str_before_username_14 + childData.email + "</strong>" + childData.data + str_after_content
                                    if(user_email == user6_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                                }           
                                document.getElementById('individual6').innerHTML = total_post6.join('');     
                            }
                        });
                    })

                    ////////////////////////////////////////7////////////////////////////////////

                    var postsRef7 = firebase.database().ref("indiv_com_list7");   
                    var str_before_username_15 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75' style='margin-left:300px;'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_before_username_16 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
                    var str_after_content = "</p></div></div>\n";
                    // List for store posts html
                    var total_post7 = [];
                    // Counter for checking history post update complete
                    var first_count_7 = 0;
                    // Counter for checking when to update new post
                    var second_count_7 = 0;
                
                    postsRef7.once('value').then(function(snapshot) 
                    {
                        total_post7.value = snapshot.val();
                        document.getElementById('individual7').innerHTML = total_post7.join('');
                        
                       
                            /// Add listener to update new post
                        postsRef7.on('child_added', function(data) 
                        {
                            second_count_7 += 1;
                            if (second_count_7 > first_count_7) 
                            {
                                var childData = data.val();
                                    //console.log(user_email + "1");
                                if(childData.email == user_email)
                                {
                                    total_post7[total_post7.length] = str_before_username_15 + childData.email + "</strong>" + childData.data + str_after_content
                                }
                                else
                                {
                                    //console.log("2");
                                    total_post7[total_post7.length] = str_before_username_16 + childData.email + "</strong>" + childData.data + str_after_content
                                    if(user_email == user7_email)
                                    {
                                        Notification.requestPermission(function(permission) 
                                        {
                                            // 使用者同意授權
                                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                                        });
                                    }
                                }           
                                document.getElementById('individual7').innerHTML = total_post7.join('');     
                            }
                        });
                    })
                }     
            });

        })
        .catch(e => console.log(e.message));

}

window.onload = function() 
{
    init();
};