function init() 
{
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) 
    {
        var User = document.getElementById('user');
        // Check user login
        if (user) 
        {
            user_email = user.email;
            User.innerHTML = "<span class='nav-link'>" + user.email;
            var logout = document.getElementById('logout');
            logout.addEventListener('click',function()
            {
                firebase.auth().signOut().then(function() 
                {
                    // Sign-out successful.
                    alert('logout successfully');
                    window.location.assign("./index.html");                                       
                }).catch(function(error) 
                {
                    // An error happened.
                    alert("logout error");
                });               
            }
            )         
        } 
    });
    
    send_btn = document.getElementById('send');
    text = document.getElementById('textbox');

    send_btn.addEventListener('click', function() 
    {
        if (text.value != "") 
        {
            console.log(text.value);
            var txt = text.value;
            firebase.database().ref("com_list").push({
                email: user_email,
                data : txt
            }); 
            text.value = "";
        }
    });
    

    var notifyConfig = 
    {
        body: 'you have a new message', // 設定內容
        icon: 'css/photo_src/tanuki.png', // 設定 icon
    };

    // The html code for post
    var postsRef = firebase.database().ref('com_list');

    var str_before_username_1 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75 float-right'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
    var str_before_username_2 = "<div class='my-3 p-3 bg-white rounded box-shadow w-75 float-left'><div class='media text-muted pt-3'><img src='css/photo_src/tanuki.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value').then(function(snapshot) 
        {
            total_post.value = snapshot.val();
            document.getElementById('post_list').innerHTML = total_post.join('');
            /// Add listener to update new post

            postsRef.on('child_added', function(data) 
            {
                second_count += 1;                 
                if (second_count > first_count) 
                {
                    var childData = data.val();
                    if(user_email == childData.email)
                        total_post[total_post.length] = str_before_username_1 + childData.email + "</strong>" + childData.data  + str_after_content
                    else
                    {
                        Notification.requestPermission(function(permission) 
                        {
                            // 使用者同意授權
                            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
                        }); 
                        total_post[total_post.length] = str_before_username_2 + childData.email + "</strong>" + childData.data  + str_after_content
                    }           
                    document.getElementById('post_list').innerHTML = total_post.join('');     
                }
            });


        })
        .catch(e => console.log(e.message));
}

window.onload = function() 
{
    init();
};