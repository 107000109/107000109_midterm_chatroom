function initApp() 
{
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfacebook');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() 
    {
        firebase.auth().signInWithEmailAndPassword (txtEmail.value, txtPassword.value).then(function(result)
        {   
            window.location = "./chatroom.html";
        }).catch(function(error)
        {
            // Handle Errors here.
            var errorMessage = error.message;
            create_alert("error",errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
        });

    });
    
    btnGoogle.addEventListener('click', function() 
    {
        var provider = new firebase.auth.GoogleAuthProvider;
        firebase.auth().signInWithPopup (provider).then(function(result)
        {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed in user info.
            var user = result.user;
            // ...
            //var errorMessage = error.message;
            //create_alert("success",errorMessage);
            window.location = "./chatroom.html";
        }).catch(function(error) 
        {
            // Handle Errors here.
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            var errorMessage = error.message;
            create_alert("error",errorMessage);
        });

    });

    btnFacebook.addEventListener('click',function()
    {
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) 
        {
            // This gives you a Facebook Access Token. You can use it to access the Facebook API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            window.location = "./chatroom.html";
            // ...
        }).catch(function(error) 
        {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            create_alert("error",errorMessage);
            // ...
        });
    })

    btnSignUp.addEventListener('click', function() 
    {
        firebase.auth().createUserWithEmailAndPassword (txtEmail.value, txtPassword.value).then(function(result)
        {
            firebase.auth().onAuthStateChanged(function(user) 
            {
                window.location = "./chatroom.html";
            }).catch(function(error)
            {
                // Handle Errors here.
                var errorMessage = error.message;
                create_alert("error",errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
        })    
    })
}
// Custom alert
function create_alert(type, message) 
{
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") 
    {
        console.log("success");
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } 
    else if (type == "error") 
    {
        console.log("error");
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() 
{
    initApp();
}
